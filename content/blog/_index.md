---
title: Blog
---

I post about programming languages, data visualization, statistical modelling,
and my recent research. Feel free to contact me if you have any question about
these posts.

